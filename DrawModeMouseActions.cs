﻿#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System;


/// <summary>
/// Determine actions preformed in Draw Map Mode
/// </summary>
public static class DrawModeMouseActions
{

    /// <summary>
    /// Sets the properties from base tile to draw tile.
    /// Uses reflection. Nearly each SpecTile needs X and Y coordinates
    /// </summary>
    /// <returns>The properties from base tile to draw tile.</returns>
    /// <param name="tile">Tile.</param>
    /// <param name="drawTile">Draw tile.</param>
    private static void SetPropertiesFromBaseTileToDrawTile (GameObject tile, GameObject drawTile)
    {
        BaseTile btile = tile.GetComponent<BaseTile> ();
        TileDrawModel drawnTile = drawTile.GetComponent<TileDrawModel> ();
        drawnTile.Renderer = drawnTile.GetComponent<SpriteRenderer> ();

        var spec = drawnTile.specTile;

        if (spec == null)
            return;

        Type type = spec.GetType ();
        //Set params, peresent only in some TideDrawModel
        try 
        {
            //Set X and Y coordinates for SpectialTile
            FieldInfo xField = type.GetField ("x", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.IgnoreCase | BindingFlags.Public);
            FieldInfo yField = type.GetField ("y", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.IgnoreCase | BindingFlags.Public);

            xField.SetValue (spec, btile.X);
            yField.SetValue (spec, btile.Y);
        }
        catch (Exception e) 
        {
            Debug.Log ("got exception : " + e);
        }
    }

    /// <summary>
    /// Pass properties from drawn tile prefab to the field tile
    /// </summary>
    /// <param name="tile">Field Tile</param>
    /// <param name="drawTile">Draw Tile</param>
    /// <param name="layer">Above or under character</param>
    private static void SetPropertiesFromDrawnTile(GameObject tile, GameObject drawTile, bool layer, bool userSet)
    {
        var baseTile = tile.GetComponent<BaseTile>();
        var drawTileModel = drawTile.GetComponent<TileDrawModel>();

        drawTileModel.UserSet = userSet;

        //Set walkable flag
        for (int i = 0; i < tile.transform.childCount; i++)
        {
            var drawtile = tile.transform.GetChild(i).GetComponent<TileDrawModel>();
            if (!drawtile.Walkable)
            {
                baseTile.Walkable = false;
                break;
            }
            baseTile.Walkable = true;
        }

        drawTileModel.OnCharacter = layer;
        drawTile.transform.localPosition = drawTileModel.DrawOffset;

        CacheTileDrawModel (baseTile, drawTileModel);

        //Debug.Log("Set drawTileProperties");
        //Find unwalkable areas
        if (drawTileModel.PartiallyWalkable)
        {
            var ua = drawTileModel.Area;
            if (ua == null)
                return;
            if (baseTile.UnwalkableAreas == null)
                baseTile.UnwalkableAreas = new List<UnwalkableArea>();

            baseTile.UnwalkableAreas.Add(ua);
            baseTile.ParitallyWalkable = true;
        }
    }


    /// <summary>
    /// Check if gameobject is an instance of a given prefab.
    /// Haven't found more reliable way so gameobject is reverted to a parent prefab,
    /// then the name of a prefab is equals check with prefab name
    /// Straight comparsion doesn't work
    /// </summary>
    /// <returns><c>true</c>, gameobject is a source prefab instance <c>false</c> otherwise.</returns>
    /// <param name="go">Gameobject</param>
    /// <param name="source">Prefab</param>
    public static bool ObjectIsPrefabInstance(GameObject go, UObject source)
    {
        if (PrefabUtility.GetPrefabType(go) != PrefabType.Prefab &&
            PrefabUtility.GetPrefabType(go) != PrefabType.PrefabInstance)
            return false;
       
        return PrefabUtility.GetPrefabParent(go).name == source.name;
    }

}

#endif
