﻿using UnityEngine;

public class IsoInput : MonoBehaviour 
{

    #region Public Variables

    /// <summary>
    /// Input source
    /// </summary>
    public JoystickController Joystick;

    /// <summary>
    /// Current direction from input
    /// </summary>
    /// <value> Caridal Directoin. </value>
    public CardialDirection Direction
    {
        get { return GetDirection(); }
    }

    /// <summary>
    /// Vector2 according to current Direction
    /// </summary>
    /// <value> The direction vector. </value>
    public Vector2 DirectionVector
    {
        get { return CardialMovement.GetCardialMovement(Direction); }
    }

    /// <summary>
    /// The speed represented by joystick input
    /// </summary>
    public float Speed
    {
        get { return GetSpeed(); }
    }

    #endregion

    #region Private Variables

    /// <summary>
    /// Coordinates of points, splitting circle in 8 parts to get the direction of movement
    /// </summary>
    private Vector2[] sectorLines = {
        new Vector2(0.75f, 0.25f),
        new Vector2(0.25f, 0.75f),
        new Vector2(-0.25f, 0.75f),
        new Vector2(-0.75f, 0.25f),
        new Vector2(-0.75f, -0.25f),
        new Vector2(-0.25f, -0.75f),
        new Vector2(0.25f, -0.75f),
        new Vector2(0.75f, -0.25f)
    };

    /// <summary>
    /// Current input cache
    /// </summary>
    private Vector2 _input;
        
    #endregion

    #region Private Methods

    /// <summary>
    /// Use the input from joystick to get the direction where it points in iso space
    /// </summary>
    /// <returns>The direction.</returns>
    private CardialDirection GetDirection()
    {
        _input = Joystick.GetInput();

        if (_input == Vector2.zero)
            return CardialDirection.None;

        //Check all sectors except North
        for (var i = 0; i < sectorLines.Length - 1; i++)
        {
            if (JoystickMath.PointInSector(sectorLines[i], sectorLines[i + 1], _input))
                return (CardialDirection)i;

        }

        return JoystickMath.PointInSector(sectorLines[sectorLines.Length - 1], sectorLines[0], _input) ?
            CardialDirection.E :
            CardialDirection.None; 
    }

    /// <summary>
    /// Joystick input is abs normalized -
    /// the sum of x and y coordinates is in bounds [0;1]
    /// So the sum of axis gives the percentage of speed
    /// </summary>
    /// <returns>The speed.</returns>
    private float GetSpeed()
    {
        _input = Joystick.GetInput();
        return Mathf.Abs(_input.x) + Mathf.Abs(_input.y);
    }

    #endregion
}
