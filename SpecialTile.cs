﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (TileDrawModel))]
[System.Serializable]
public abstract class SpecialTile : MonoBehaviour 
{
    #region Public Fields

    public int X;
    public int Y;
    public int Radius;
    public bool IsActive = true;
    public bool HasItem;
    public bool HasModifier;

    public Collider2D CollideArea;
    public SpriteRenderer Renderer;

    #endregion

    #region PrivateMethods

    protected virtual void Start ()
    {
        if (Renderer == null)
            Renderer = GetComponent<SpriteRenderer> ();
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Check if it's time to interact with player
    /// </summary>
    /// <returns>The player is in radius.</returns>
    /// <param name="x">The x coordinate of player.</param>
    /// <param name="y">The y coordinate of player.</param>
    public virtual bool CheckPlayerInRadius (int x, int y)
    {
        var topBorder = x + Radius;
        var bottomBorder = x - Radius;
        if (X > topBorder || X < bottomBorder)
            return false;
        topBorder = y + Radius;
        bottomBorder = y - Radius;
        if (Y > topBorder || Y < bottomBorder)
            return false;
        return true;
    }

    /// <summary>
    /// Activate the special effect of a tile
    /// </summary>
    public virtual void Activate (MovementController player = null)
    {
        if (!IsActive)
            return;
        IsActive = false;

    }

    /// <summary>
    /// Returns item on the tile.
    /// Base implementation returns nonexistant item
    /// </summary>
    /// <returns>The item.</returns>
    public virtual ItemModel GiveItem ()
    {
        return new ItemModel { ItemId = -1, Count = 0 };
    }

    /// <summary>
    /// Fade animation
    /// </summary>
    /// <returns>The object.</returns>
    protected virtual IEnumerator HideObject ()
    {
        while (Renderer.color.a > 0)
        {
            Color col = Renderer.color;
            col.a -= 0.25f;
            Renderer.color = col;
            yield return new WaitForSeconds (0.1f);
        }
        Renderer.enabled = false;
        if (CollideArea != null)
            CollideArea.enabled = false;
    }

    #endregion
}
