﻿#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Linq;

public class FloorRandomizerWindow : EditorWindow
{
    #region Public Variables

    public FloorRandomizerHelper FloorHelper;

    #endregion

    #region Private Variables

    private static FloorRandomizerWindow _instance;

    private Vector2 _scrollPosition;

    private float TileYSize {
        get { return Mathf.Abs (FloorHelper.Map.TileSet [1].transform.position.y - FloorHelper.Map.TileSet [0].transform.position.y); }
    }

    /// <summary>
    /// Style to draw text fields
    /// </summary>
    /// <value>The label style.</value>
    private GUIStyle BigLabelStyle {
        get {
            if (_labelStyle == null) {
                var style = new GUIStyle ();
                style.alignment = TextAnchor.UpperCenter;
                style.fontStyle = FontStyle.Bold;
                style.fontSize = 13;

                _labelStyle = style;
            }
            return _labelStyle;
        }
        set {
            _labelStyle = value;
        }

    }

    private GUIStyle _labelStyle;

    #endregion

    #region Public Methods

    public static FloorRandomizerWindow Show ()
    {
        if (_instance == null)
            return null;
        else
            EditorWindow.FocusWindowIfItsOpen<FloorRandomizerWindow> ();
        return _instance;
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Create new instance
    /// </summary>
    public static FloorRandomizerWindow Initialize (TiledMap map)
    {
        if (_instance != null)
            return _instance;

        FloorRandomizerWindow window = EditorWindow.GetWindow<FloorRandomizerWindow> ("Randomizer", false);
        window.FloorHelper = new FloorRandomizerHelper (map);
        _instance = window;

        return _instance;
    }

    /// <summary>
    /// Draw contents of the window
    /// </summary>
    private void OnGUI ()
    {
        DrawAddButton ();

        EditorGUILayout.LabelField ("Actions", BigLabelStyle);

        if (GUILayout.Button ("Normalize Probabilities")) {
            NormalizeProbabilities ();
        }

        DrawRemoveSelection ();

        DrawRandomize ();

        if (GUILayout.Button ("Remove random floor")) {
            RemoveRandomFloor ();
        }

        GUILayout.Label ("", GUI.skin.horizontalSlider);

        DrawSelectedFloors ();
    }

    /// <summary>
    /// Draws fields for a new ground tile selection and "Add" button.
    /// Button adds selected ground tile to a list of ground tile which
    /// will take place in random generation.
    /// </summary>
    private void DrawAddButton ()
    {
        //Field for adding new floor
        EditorGUILayout.LabelField ("Select Floor Tile", BigLabelStyle);
        EditorGUILayout.LabelField ("Select Floor to add");
        FloorHelper.NewFloorObject = EditorGUILayout.ObjectField (FloorHelper.NewFloorObject, typeof (GameObject), false);
        FloorHelper.NewFloorPorbability = EditorGUILayout.IntField ("Probability: ", FloorHelper.NewFloorPorbability);

        if (FloorHelper.NewFloorObject != null)
        {
            var texture = AssetPreview.GetAssetPreview (FloorHelper.NewFloorObject);
            if (texture == null)
                GUILayout.Label ("No preview");
            else
            {
                GUILayout.Label ("Preview: ");
                GUILayout.Label (texture);
            }
        }

        //Add new Floor tdm to randomizer
        if (GUILayout.Button ("Add"))
        {
            if (FloorHelper.NewFloorObject == null)
                Debug.LogError ("Select floor prefab to add");
            else
            {
                var selectedFloors = from floor in FloorHelper.Floors select floor.Tdm;
                if (selectedFloors.Contains (FloorHelper.NewFloorObject)) 
                {
                    Debug.LogError ("Selected floor is already in list");
                    return;
                }

                var newFloorTdm = new FloorEditorSpecial ();
                newFloorTdm.Tdm = FloorHelper.NewFloorObject;
                FloorHelper.NewFloorObject = null;
                newFloorTdm.Probability = FloorHelper.NewFloorPorbability;
                FloorHelper.NewFloorPorbability = 0;
                FloorHelper.Floors.Add (newFloorTdm);
            }
        }
        GUILayout.Label ("", GUI.skin.horizontalSlider);
    }

    /// <summary>
    /// Draw floor tdms already added to randomizer and their preview
    /// </summary>
    /// <returns>The selected floors.</returns>
    private void DrawSelectedFloors ()
    {
        EditorGUILayout.LabelField ("Selected Floors", BigLabelStyle);

        if (FloorHelper == null || FloorHelper.Floors == null)
            return;

        GUILayout.Label (string.Format ("Total Floors Selected :" + FloorHelper.Floors.Count));

        _scrollPosition = EditorGUILayout.BeginScrollView (_scrollPosition);

        bool remove = false;
        int removeIdx = 0;

        //Floor helper "Editor" view
        for (int i = 0; i < FloorHelper.Floors.Count; i++)
        {
            var floorTdm = FloorHelper.Floors [i];
            GUILayout.Label (string.Format ("Number :" + (i + 1)));
            floorTdm.Tdm = EditorGUILayout.ObjectField (floorTdm.Tdm, typeof (GameObject), false);
            floorTdm.Probability = EditorGUILayout.IntField ("Probability :", floorTdm.Probability);
            if (floorTdm.Preview != null) 
            {
                GUILayout.Label ("Preview :");
                GUILayout.Label (floorTdm.Preview);
            }

            if (GUILayout.Button ("Remove")) 
            {
                remove = true;
                removeIdx = i;
            }
            GUILayout.Label ("   ");
        }

        EditorGUILayout.EndScrollView ();

        //Remove button pressed on one of selected floors
        if (remove)
        {
            remove = false;
            FloorHelper.Floors.RemoveAt (removeIdx);
        }
    }


    /// <summary>
    /// Create random floor
    /// Randomize button checks probability normalization, then creates and draws
    /// random ground (floor) on the map according to given probabilities list.
    ///  Fills only non userSet tiles.  All previously programmaticaly set ground tiles will be removed.
    /// </summary>
    private void DrawRandomize ()
    {
        if (GUILayout.Button ("Randomize"))
        {
            //Sanity checks
            if (FloorHelper.Floors == null ||
               FloorHelper.Floors.Count == 0) 
            {
                Debug.LogError ("Randomization cancelled. No floors selected");
                return;
            }

            NormalizeProbabilities ();
            RemoveRandomFloor ();

            foreach (var baseTile in FloorHelper.Map.TileSet) 
            {
                if (baseTile.UnderCharacterTiles == null)
                    baseTile.UnderCharacterTiles = new List<TileDrawModel> ();
                
                //Check if there is no IsGround non userSet tiles on baseTile
                if (baseTile.UnderCharacterTiles.Any (x => x.IsGround && x.UserSet))
                        continue;
                var selectedFloor = GetRandomFloorFromList ();

                if (selectedFloor == null)
                {
                    Debug.Log ("No floor by probability found");
                    return;
                }

                //Draw tdm
                var tdm = DrawModeMouseActions.LeftMouseButton (baseTile.gameObject, selectedFloor.Tdm, false, false);
                IsometricTileUtility.SetTdmZindex (TileYSize, baseTile, tdm);
            }
            EditorSceneManager.MarkSceneDirty (SceneManager.GetActiveScene ());
        }
    }

    /// <summary>
    /// Generate a random int in [0,100] bounds and pick a floor (ground tdm)
    /// from selected list according to their probabilites.
    /// </summary>
    /// <returns>The random floor (ground) tdm</returns>
    private FloorEditorSpecial GetRandomFloorFromList ()
    {
        //Get random floor
        int rnd = Random.Range (0, 100);
        //Bounds for the current floor
        int lower, higher = 0;

        FloorEditorSpecial retVal = null;
        for (int i = 0; i < FloorHelper.Floors.Count; i++) 
        {
            lower = higher;
            higher += FloorHelper.Floors [i].Probability;

            if (rnd >= lower && rnd < higher) 
            {
                retVal = FloorHelper.Floors [i];
                break;
            }
        }
        return retVal;
    }

    /// <summary>
    /// Remove previously random generated ground. 
    /// All userSet grounds aren't affected.
    /// </summary>
    private void RemoveRandomFloor ()
    {
        //Sanity checks
        if (FloorHelper.Map.TileSet == null || FloorHelper.Map.TileSet.Count == 0)
            return;

        foreach (var baseTile in FloorHelper.Map.TileSet)
        {
            var indexes = new List<int> ();

            if (baseTile.UnderCharacterTiles == null || baseTile.UnderCharacterTiles.Count == 0)
                continue;
            
            for (int i = 0; i < baseTile.UnderCharacterTiles.Count; i++) 
            {
                var underTile = baseTile.UnderCharacterTiles [i];
                if (underTile.IsGround && !underTile.UserSet)
                    indexes.Add (i);
            }

            foreach (var idx in indexes)
            {
                var go = baseTile.UnderCharacterTiles [idx].gameObject;
                baseTile.UnderCharacterTiles.RemoveAt (idx);
                DestroyImmediate (go);
            }
        }
        EditorSceneManager.MarkSceneDirty (SceneManager.GetActiveScene ());
    }

    private void DrawRemoveSelection ()
    {
        if(GUILayout.Button("Clear all selected"))
            FloorHelper.Floors.Clear ();
    }
    #endregion
}

#endif
